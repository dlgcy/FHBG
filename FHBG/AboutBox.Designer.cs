﻿namespace dlgcy
{
    partial class AboutBox
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutBox));
            this.LblProductName = new System.Windows.Forms.Label();
            this.TBDescription = new System.Windows.Forms.RichTextBox();
            this.okButton = new System.Windows.Forms.Button();
            this.LblCompanyName = new System.Windows.Forms.Label();
            this.LblInfoVersion = new System.Windows.Forms.Label();
            this.logoPictureBox = new System.Windows.Forms.PictureBox();
            this.LblVersion = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LblUpdateTitle = new System.Windows.Forms.Label();
            this.LblWebSiteTitle = new System.Windows.Forms.Label();
            this.LinkWebSite = new System.Windows.Forms.LinkLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LblWeChatBlogTitle = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // LblProductName
            // 
            this.LblProductName.AutoSize = true;
            this.LblProductName.Location = new System.Drawing.Point(409, 14);
            this.LblProductName.Margin = new System.Windows.Forms.Padding(9, 0, 4, 0);
            this.LblProductName.MaximumSize = new System.Drawing.Size(0, 24);
            this.LblProductName.Name = "LblProductName";
            this.LblProductName.Size = new System.Drawing.Size(62, 18);
            this.LblProductName.TabIndex = 19;
            this.LblProductName.Text = "名称：";
            this.LblProductName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TBDescription
            // 
            this.TBDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBDescription.Location = new System.Drawing.Point(412, 254);
            this.TBDescription.Margin = new System.Windows.Forms.Padding(4);
            this.TBDescription.Name = "TBDescription";
            this.TBDescription.Size = new System.Drawing.Size(899, 694);
            this.TBDescription.TabIndex = 25;
            this.TBDescription.Text = "";
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.okButton.Location = new System.Drawing.Point(1199, 969);
            this.okButton.Margin = new System.Windows.Forms.Padding(4);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(112, 32);
            this.okButton.TabIndex = 24;
            this.okButton.Text = "确定(&O)";
            // 
            // LblCompanyName
            // 
            this.LblCompanyName.AutoSize = true;
            this.LblCompanyName.Location = new System.Drawing.Point(409, 134);
            this.LblCompanyName.Margin = new System.Windows.Forms.Padding(9, 0, 4, 0);
            this.LblCompanyName.MaximumSize = new System.Drawing.Size(0, 24);
            this.LblCompanyName.Name = "LblCompanyName";
            this.LblCompanyName.Size = new System.Drawing.Size(62, 18);
            this.LblCompanyName.TabIndex = 22;
            this.LblCompanyName.Text = "作者：";
            this.LblCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LblInfoVersion
            // 
            this.LblInfoVersion.AutoSize = true;
            this.LblInfoVersion.Location = new System.Drawing.Point(409, 94);
            this.LblInfoVersion.Margin = new System.Windows.Forms.Padding(9, 0, 4, 0);
            this.LblInfoVersion.MaximumSize = new System.Drawing.Size(0, 24);
            this.LblInfoVersion.Name = "LblInfoVersion";
            this.LblInfoVersion.Size = new System.Drawing.Size(116, 18);
            this.LblInfoVersion.TabIndex = 21;
            this.LblInfoVersion.Text = "产品版本号：";
            this.LblInfoVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // logoPictureBox
            // 
            this.logoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("logoPictureBox.Image")));
            this.logoPictureBox.Location = new System.Drawing.Point(4, 4);
            this.logoPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.logoPictureBox.Name = "logoPictureBox";
            this.logoPictureBox.Size = new System.Drawing.Size(392, 608);
            this.logoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logoPictureBox.TabIndex = 12;
            this.logoPictureBox.TabStop = false;
            // 
            // LblVersion
            // 
            this.LblVersion.AutoSize = true;
            this.LblVersion.Location = new System.Drawing.Point(409, 54);
            this.LblVersion.Margin = new System.Windows.Forms.Padding(9, 0, 4, 0);
            this.LblVersion.MaximumSize = new System.Drawing.Size(0, 24);
            this.LblVersion.Name = "LblVersion";
            this.LblVersion.Size = new System.Drawing.Size(62, 18);
            this.LblVersion.TabIndex = 0;
            this.LblVersion.Text = "版本：";
            this.LblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LblWeChatBlogTitle);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.LinkWebSite);
            this.panel1.Controls.Add(this.LblWebSiteTitle);
            this.panel1.Controls.Add(this.LblUpdateTitle);
            this.panel1.Controls.Add(this.okButton);
            this.panel1.Controls.Add(this.TBDescription);
            this.panel1.Controls.Add(this.LblInfoVersion);
            this.panel1.Controls.Add(this.logoPictureBox);
            this.panel1.Controls.Add(this.LblCompanyName);
            this.panel1.Controls.Add(this.LblProductName);
            this.panel1.Controls.Add(this.LblVersion);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(14, 12);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1325, 1005);
            this.panel1.TabIndex = 1;
            // 
            // LblUpdateTitle
            // 
            this.LblUpdateTitle.AutoSize = true;
            this.LblUpdateTitle.Location = new System.Drawing.Point(409, 214);
            this.LblUpdateTitle.Name = "LblUpdateTitle";
            this.LblUpdateTitle.Size = new System.Drawing.Size(98, 18);
            this.LblUpdateTitle.TabIndex = 26;
            this.LblUpdateTitle.Text = "更新内容：";
            // 
            // LblWebSiteTitle
            // 
            this.LblWebSiteTitle.AutoSize = true;
            this.LblWebSiteTitle.Location = new System.Drawing.Point(409, 174);
            this.LblWebSiteTitle.Name = "LblWebSiteTitle";
            this.LblWebSiteTitle.Size = new System.Drawing.Size(98, 18);
            this.LblWebSiteTitle.TabIndex = 27;
            this.LblWebSiteTitle.Text = "官网地址：";
            // 
            // LinkWebSite
            // 
            this.LinkWebSite.AutoSize = true;
            this.LinkWebSite.Location = new System.Drawing.Point(513, 174);
            this.LinkWebSite.Name = "LinkWebSite";
            this.LinkWebSite.Size = new System.Drawing.Size(134, 18);
            this.LinkWebSite.TabIndex = 28;
            this.LinkWebSite.TabStop = true;
            this.LinkWebSite.Text = "dlgcy.com/fhbg";
            this.LinkWebSite.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkWebSite_LinkClicked);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::dlgcy.Properties.Resources.weixin_dlgcy_blog;
            this.pictureBox1.InitialImage = global::dlgcy.Properties.Resources.weixin_dlgcy_blog;
            this.pictureBox1.Location = new System.Drawing.Point(79, 727);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(221, 220);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 29;
            this.pictureBox1.TabStop = false;
            // 
            // LblWeChatBlogTitle
            // 
            this.LblWeChatBlogTitle.AutoSize = true;
            this.LblWeChatBlogTitle.Font = new System.Drawing.Font("楷体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LblWeChatBlogTitle.Location = new System.Drawing.Point(28, 660);
            this.LblWeChatBlogTitle.Name = "LblWeChatBlogTitle";
            this.LblWeChatBlogTitle.Size = new System.Drawing.Size(335, 24);
            this.LblWeChatBlogTitle.TabIndex = 30;
            this.LblWeChatBlogTitle.Text = "微信公众号：独立观察员博客";
            // 
            // AboutBox
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.okButton;
            this.ClientSize = new System.Drawing.Size(1353, 1029);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimizeBox = false;
            this.Name = "AboutBox";
            this.Padding = new System.Windows.Forms.Padding(14, 12, 14, 12);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "关于";
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblProductName;
        private System.Windows.Forms.RichTextBox TBDescription;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label LblCompanyName;
        private System.Windows.Forms.Label LblInfoVersion;
        private System.Windows.Forms.PictureBox logoPictureBox;
        private System.Windows.Forms.Label LblVersion;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LblUpdateTitle;
        private System.Windows.Forms.Label LblWebSiteTitle;
        private System.Windows.Forms.LinkLabel LinkWebSite;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label LblWeChatBlogTitle;
    }
}
